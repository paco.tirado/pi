#!/bin/bash
# Limpiamos la pantalla
clear

# Lo primero es prepara el nombre de archivo donde guardar el log.
# Usamos la fecha para el nombre del archivo AñoMesDia-HoraMinuto
# El nombre lo hacemos combinando 3 variables en una 4. 1 para la
# ruta del archivo, la 2a para la parte variable del nombre
# y la 3a con la extension. En la última lo combinamos todo en una.
# asegurate que has creado la carpeta logs en el usuario pi
# de lo contrario dará un error y no funcionará
ruta="/home/pacanis/logs/Actualizacion_"
nombre_archivo=$(date +%Y%m%d-%H%M)
extension=".log"
ruta_completa=$ruta$nombre_archivo$extension

# Creamos el archivo. El propietario es el root, pero se puede leer
# con cualquier usuario del sistema. No se puede modificar.
# las variables se declaran sin nada, pero para llamarlas se usa 
# $delante del nombre
touch $ruta_completa

#
# Añadimos la primera línea. Añadimos la fecha y hora de inicio
#
echo "########################################################" >> $ruta_completa
echo "Iniciando actualización... $(date)" >> $ruta_completa
echo "########################################################" >> $ruta_completa

#
# Empezamos la actualización
#
echo "" >> $ruta_completa
echo "Actualizando lista de paquetes..." >> $ruta_completa
apt-get update >> $ruta_completa

#
# Actualizando sistema, sin preguntar parámetro -y
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Actualizando paquetes..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
#
# Actualizar sistema
#
apt-get -y upgrade >> $ruta_completa

#
# Subir de versión
#
#apt-get -y dist-upgrade >> $ruta_completa

#
# Borrando paquetes antiguos, sin preguntar parámetro -y
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Borrando paquetes antiguos/obsoletos..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
apt-get -y autoremove >> $ruta_completa

#
# Borrando archivos innecesarios
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Borrando paquetes descargados..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
apt-get -y autoclean >> $ruta_completa

#
# Comprobando versión kernel
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Comprobando versión kernel..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
uname -a >> $ruta_completa

#
# Actualizando Kernel y Firmware
#
#echo "" >> $ruta_completa
#echo "########################################################" >> $ruta_completa
#echo "Actualizando Kernel y Firmware..." >> $ruta_completa
#echo "########################################################" >> $ruta_completa

#sleep 20
#rpi-update >> $ruta_completa

#
# Actualizando Telegram
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Actualizando Telegram..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
pip3 install pytelegrambotapi --upgrade >> $ruta_completa

#
# Actualizando Pi-hole
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Actualizando Pi-hole..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
pihole -up >> $ruta_completa

#
# Actualizando listas Pi-hole
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Actualizando listas de bloqueo Pi-hole..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
pihole -g >> $ruta_completa

#
# Borrando log Pi-hole
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Borrando log Pi-hole..." >> $ruta_completa
echo "########################################################" >> $ruta_completa
pihole flush >> $ruta_completa

#
# Escribimos la última línea. Añadimos la fecha y hora de final
#
echo "" >> $ruta_completa
echo "########################################################" >> $ruta_completa
echo "Actualización completa. $(date)" >> $ruta_completa
echo "########################################################" >> $ruta_completa
#
# Nos envia una notificación usando nuestro bot de telegram
#
python /root/scripts/BotTelegramNotificaciones.py '2' $ruta_completa

#
# Avisamos de actualizar a mano si pone "www.raspberrypi.org"
#
if grep -e www.raspberrypi.org $ruta_completa; then

#
# Nos envia una notificación usando nuestro bot de telegram
#
python /root/scripts/BotTelegramNotificaciones.py '3' $ruta_completa

fi

#
# Rebotamos si pone "reboot"
#
if grep -e reboot $ruta_completa; then
reboot
fi
