#!/bin/bash

# Mata todos los procesos de python activos
killall python

# Espera 3 segundos
sleep 3

# Reinicia el bot de telegram
python /root/scripts/BotTelegram.py &
