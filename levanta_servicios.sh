#!/bin/bash

# Mata todos los procesos de python activos
killall python

# Arrancamos Firewall
ufw enable

# Limpiamos la pantalla
clear

# Lo primero es preparar el nombre de archivo donde guardar el log.
# En este caso creamos solo un archivo sobre el que iremos añadiendo
# la información.
ruta_completa="/home/pacanis/logs/Reinicios.log"

#
# Comprobando versión kernel
#
echo "" >> $ruta_completa
echo "Comprobando versión kernel..." >> $ruta_completa
uname -a >> $ruta_completa
echo "" >> $ruta_completa

# Escribimos la línea dejando constancia del reinicio 
# Añadimos la fecha y hora de final
echo "$(date) > Reinicio del sistema." >> $ruta_completa
echo "===============================================================" >> $ruta_completa

#
# Nos envia una notificación usando nuestro bot de telegram
#
python /root/scripts/BotTelegramNotificaciones.py '1' $ruta_completa

# Espera 3 segundos
sleep 3

# Reinicia el bot de telegram
python /root/scripts/BotTelegram.py &
