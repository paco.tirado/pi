#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# IMPORTAMOS LAS LIBRERIAS QUE VAMOS A NECESITAR PARA NUESTRO BOT
#
import telebot
from telebot import types
import time
import os
import sys
import subprocess
import random
from random  import choice
import string
import traceback

#
# Variable para gestionar las excepciones que se envian por telegram
#
enviar_error = 0

# Con está instrucción aumentamos el máximo de llamadas recursivas, por defecto (1000) da un error
# El bot se cierra de vez en cuando, pese a esta línea. En mi crontab tengo un script que apaga
# y enciende el bot cada 15 minutos. De esta forma me aseguro que siempre está activo.
sys.setrecursionlimit(3000)

#
# Este es nuestro token para el bot
#
archivoToken = open('/root/token.txt', 'r')
TOKEN = str(archivoToken.readline().rstrip('\n'))

# Chatid, la forma más fácil de obtenerlo es envia un mensaje al canal y entrando
# en esta página https://api.telegram.org/bot<TOKEN>/getUpdates?offset=0
archivoChatid = open('/root/id.txt', 'r')
chatid = str(archivoChatid.readline().rstrip('\n'))

#
# Cerramos los archivos
#
archivoToken.close()
archivoChatid.close()

# Preparamos nuestro bot. Todas las acciones se hacen con la clase 'telebot.' lo que sea.
# La cargamos en un objeto 'tb', que es el que usaremos siempre para llamar a los diferentes
# metodos de la clase.
tb = telebot.TeleBot(TOKEN,threaded=False,skip_pending=False)

# Las funciones son bloques de código independiente que se usan bajo demanda. En este ejemplo
# vamos a crear varias. Las 6 primeras son para recuperar información del sistema.
# Se inician con la palabra reservada 'def' con el nombre de la función y (sus parametros si los necesita)

#
# INFO ARQUITECTURA CPU
#
def infoArqCPU():
	lectura = os.popen('lscpu | grep Architecture').readline()
	return lectura.split()[1:2]

#
# INFO NÚMERO CPU(s)
#
def infoNumCPU():
	lectura = os.popen('lscpu | grep \'CPU(s):\'').readline()
	return lectura.split()[1:3]

#
# INFO RAM
#
# Esta función captura la memoria RAM del sistema que necesitaremos un poco más abajo
def ramInfo():
	# En la variable 'p' almacenamos el resultado del comando free
	p = os.popen('free -h')
        i = 0

        # Con este bucle recogemos la línea recuperada del comando free
        # y la guardamos en la variable 'line'
        while 1:
                i += 1
                line = p.readline()

                # Con este condicional nos aseguramos que la instrucción siguiente
                # se ejecuta cuando hemos capturado la línea
                if i == 2:
                        # Con esta instrucción devolvemos un array (matriz) con
                        # los diferentes valores de la linea.
                        # Basicamente line.split devuelve una lista con todas las
                        # palabras de una frase (quita los espacios).
                        return(line.split()[1:4])

#
# INFO HD
#
# Con esta función capturamos el espacio de disco (SD), funciona igual que la anterior
def diskSpace():
        p = os.popen('df -h /')
        i = 0
        while 1:
                i += 1
                line = p.readline()

                # Con este condicional nos aseguramos que la instrucción siguiente
                # se ejecuta cuando hemos capturado la línea
                if i == 2:
                        # Con esta instrucción devolvemos un array (matriz) con
                        # los diferentes valores de la linea.
                        # Basicamente line.split devuelve una lista con todas las
                        # palabras de una frase (quita los espacios).
                        return(line.split()[1:5])

# Los bots de telegram admiten comandos para interactuar con ellos.
#   ayuda - Muestra información de asistencia
#   ping - Comprueba que el servidor esta activo
#   info - Muestra información del sistema
#   ip - Envía la dirección de ip pública del servidor
#   servicios - Comprueba el estado de algunos servicios
#   vpn - Muestra el estado actual del servidor OpenVPN
#   reinicio - Reinicia el servidor
#   menu - Muestra los botones


#
# Comando AYUDA
#
@tb.message_handler(commands=['ayuda'])

def comando_ayuda(mensaje):
        tb.send_message(chatid, '/ayuda - Muestra esta información de asistencia\n/ping - Comprueba que el servidor sigue activo\n/info - Muestra información del sistema\n/ip - Envía la dirección de ip pública del servidor\n/servicios - Comprueba el estado de algunos servicios\n/vpn - Muestra el estado actual del servidor OpenVPN\n/reinicio - Reinicia el servidor\n/menu - Muestra los botones\n',None)

#
# Comando PING
#
@tb.message_handler(commands=['ping'])

def comando_ping(mensaje):
	tb.send_message(chatid, '¡¡¡Sigo vivo!!!\n\n'+os.popen('uptime -p').read(),None)

#
# Comando INFO
#
@tb.message_handler(commands=['info'])

def comando_info(mensaje):
	#
        # Obtenemos la TEMPERATURA DE LA CPU
	#

        # Preparamos la ruta del archivo donde se guarda la temperatura
        tempFile = open( '/sys/class/thermal/thermal_zone0/temp' )

        # Leemos el archivo de la temperatura y lo guardamos en un variable
        cpu_temp = tempFile.read()

        # Cerramos el archivo
        tempFile.close()

        # Convertimos la temperatura en grados. Con la intrucción
        # float forzamos la variable cpu_temp como un numero decimal
        # con round hacemos que redondee la división y con la
        # instrucción str lo convertimos en texto.
        # Es necesario pasarlo a texto para poder concatenarlo con el
        # resto del mensaje.
        cpu_temp = str(round(float(cpu_temp)/1000))

	cpu_arqu = str(infoArqCPU())
	cpu_arqu = cpu_arqu[2:-2]

	cpu_proc = str(infoNumCPU())
	cpu_proc = cpu_proc[2:-2]

	#
        # MEMORIA RAM
	#

        # Asignamos a las diferentes variables el valor almacenado en
        # la posición del array que devuelve la función ramInfo que
        # hemos creado antes.
        # Entre [] establecemos la posición del array que nos devuelve
        # la primera posición es siempre 0, que corresponde a la 1ª
        # palabra de la frase.
        totalRam = ramInfo()[0]
        usadoRam = ramInfo()[1]
        libreRam = ramInfo()[2]

        # Disco duro
        # Funciona igual que la anterior.
        totalHD = diskSpace()[0]
        usadoHD = diskSpace()[1]
        libreHD = diskSpace()[2]
        porceHD = diskSpace()[3]

        # Información general

        # Capturamos el sistema
	unameo = os.popen('uname -o').read()

        # Capturamos la máquina
	unamen = os.popen('uname -n').read()

        # Capturamos el tiempo de actividad del sistema
        uptime = os.popen('uptime -p').read()

        # Con esta instrucción manipulamos la cadena. Si te fijas una cadena
        # no es más que una matriz de caracteres. Lo que aqui hacemos es decirle
        # coge de la cadena almacenada en la variable uptime desde el caracter 0,
        # 2 posiciones (uptime[0:2]), añade el texto 'time: ' (+'time: '+) y
        # añade la parte de la cadena uptime desde la posición 3 hasta el final
        # (uptime[3:]). Y todo esto lo guardas en la variable uptime (uptime =)
        # Los textos, son literales y deben ir entre comillas, simples o dobles,
        # pero entre comillas. ' ' o ' '.
        uptime = uptime[0:2]+'time: '+uptime[3:]

	tb.send_message(chatid, 'INFORMACIÓN DEL SISTEMA\n\n     Sistema: '+unameo+'     Máquina: '+unamen+'\n- Información CPU:\n     Temperatura: '+cpu_temp+'º C\n     Arquit.: '+cpu_arqu+'\n     Nº procesad.: '+cpu_proc+'\n\n- RAM total '+totalRam+':\n     Usado: '+usadoRam+'\n     Libre: '+libreRam+'\n\n- Tarjeta SD '+totalHD+' ('+porceHD+')\n     Usado: '+usadoHD+'\n     Libre: '+libreHD,None)

#
# Comando IP
#
@tb.message_handler(commands=['ip'])

def comando_ip(mensaje):
        # Descarga la línea donde esta nuestra ip en la web que nos interesa
        # si quieres usar otra web tendrás que adaptar el script
        cadena_ip = os.popen('curl https://www.showmyip.gr/ | grep -i \'Your IP is: <span class=\'').read()
        # Construye la cadena que nos va a enviar (omitiendo las primeras 49 posiciones)
        cadena_ip = 'La ip pública del servidor es: '+str(cadena_ip[49:])

        tb.send_message(chatid, cadena_ip, None)

#
# Comando SERVICIOS
#
@tb.message_handler(commands=['servicios'])

def comando_servicios(mensaje):
	# Comprobamos los diferentes servicios, este comando nos da muchas lineas, para
	# eso usamos el pipe (|) grep con el parametro Active, lo que captura solo la línea
	# que nos interesa
	ngi = os.popen('systemctl status nginx.service      | grep Active').readline()
	sql = os.popen('systemctl status mariadb.service    | grep Active').readline()
	fai = os.popen('systemctl status fail2ban.service   | grep Active').readline()
	php = os.popen('systemctl status php7.3-fpm.service | grep Active').readline()
	vpn = os.popen('systemctl status openvpn.service    | grep Active').readline()
        pih = os.popen('systemctl status pihole-FTL.service | grep Active').readline()
        ufw = os.popen('systemctl status ufw.service        | grep Active').readline()

	# Desechamos los primeros 11 caracteres
	ngi = ngi[11:]
	sql = sql[11:]
	fai = fai[11:]
	php = php[11:]
	vpn = vpn[11:]
	pih = pih[11:]
	ufw = ufw[11:]

	tb.send_message(chatid, 'ESTADO DE LOS PRINCIPALES SERVICIOS\n\n- Nginx: '+ngi+'- MariaDB: '+sql+'- Fail2ban: '+fai+'- PHP 7: '+php+'- OpenVPN: '+vpn+'- Pi-hole: '+pih+'- Ufw: '+ufw)

#
# Comando VPN
#
@tb.message_handler(commands=['vpn'])

def comando_vpn(mensaje):
	cConectados=os.popen('pivpn -c').read()
        lClientes=os.popen('pivpn -l').read()

        tb.send_message(chatid, 'CLIENTES CONECTADOS\n'+cConectados+'\nLista de clientes:\n'+lClientes,None)

#
# Comando MENU
#
@tb.message_handler(commands=['menu'])

def comando_menu(mensaje):
#
# Teclado de acciones
#
	markup = types.ReplyKeyboardMarkup()
	markup.row('/ayuda', '/ping', '/info')
	markup.row('/ip', '/servicios', '/vpn')
	markup.row('/reinicio')
	tb.send_message(chatid, 'Comandos disponibles', reply_markup=markup)

#
# Comando REINICIO
#
@tb.message_handler(commands=['reinicio'])

def comando_reinicio(mensaje):
        log_reinicio=open('/home/pacanis/logs/Reinicios.log','a')
        hora=time.strftime('%c')
        log_reinicio.write('\nReinicio forzado via Telegram > '+hora+'\n')
        log_reinicio.close()
        os.popen('shutdown -r now')

# Comando de prueba no listado, se debe teclear a mano
# imprime el mensaje que ha recibido el bot por la
# pantalla de la terminal. No tiene sentido enviarlo
# si no estás conectado al servidor ya que no obtendras
# respuesta del bot.
@tb.message_handler(commands=['prueba'])

def comando_prueba(mensaje):
	print 'Imprimiendo mensaje en pantalla:'
	print mensaje

#
# ESTA FUNCIÓN INTENTA MANTENER EL MODO 'VIVO' DEL BOT
#
def telegram_activo():
        # Hace un intento, si no funciona captura el error (o excepción) y lo guarda en un log
         try:
                tb.polling(none_stop = True, timeout=60)

         except:
                trazar_excepcion=traceback.format_exc()
                tb.send_message(chatid, 'Error de <polling>polling(none_stop = True, timeout=60):\n\n'+trazar_excepcion, None)

                # Prepara fecha y hora para añadirla al mensaje
                hora=time.strftime('%c')

		# Prepara el threadName
		#threadName = threading.currentThread().getName()

		# Prepara el activeThreads
                #activeThreads = threading.activeCount() - 1

                # Abre archivo de log
                log=open('/home/pacanis/logs/Error_telegram.log','a')

                # Escribe el error en el archivo de log
                #log.write('\n'+str(hora)+' || chatid > '+str(chatid)+' || threadName '+str(threadName)+ ' || activeThreads '+activeThreads+ ' || Error de <polling> (none_stop = True, timeout=60)\n   '+str(trazar_excepcion)+'\n\n============================\n')
                log.write('\n'+str(hora)+' || chatid > '+str(chatid)+' || Error de <polling> (none_stop = True, timeout=60)\n   '+str(trazar_excepcion)+'\n\n============================\n')

                # Cierra el archivo de log
                log.close()

                # Detiene el modo 'vivo' del bot
                tb.stop_polling()

                # Pausa durante 10 segundos la ejecución del programa
                time.sleep(10)

                # Intenta reactivar el modo 'vivo' del bot
                telegram_activo()

#
# Llama a la función anterior para mantener el bot activo
#
if __name__ == '__main__':
         telegram_activo()
