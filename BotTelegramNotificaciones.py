#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este script sirve para enviar las notificaciones de las actuaciones diarias
# de mantenimiento. Esta limitado a un solo chat por el chatid, de esta forma
# solo yo recibire las notificaciones. Si borras el chat y lo abres de nuevo,
# por ejemplo si cambias de móvil, asegurate de cambiar el chatid.

import telebot
import sys
import os

# Como tengo hecho un script para hacer commit de los cambios en git si
# subo los archivos a pelo subire tanto el token como el chatid, de esta
# forma evito que se suban y mantengo el script limpio de datos sensibles

#
# Este es nuestro token para el bot
#
archivoToken = open('/root/token.txt', 'r')
TOKEN = str(archivoToken.readline().rstrip('\n'))

# Chatid, la forma más fácil de obtenerlo es envian un mensaje al canal y entrando
# en esta página https://api.telegram.org/bot<TOKEN>/getUpdates?offset=0
archivoChatid = open('/root/id.txt', 'r')
chatid = str(archivoChatid.readline().rstrip('\n'))

#
# Cerramos los archivos
#
archivoToken.close()
archivoChatid.close()

# Preparamos nuestro bot. Todas las acciones se hacen con la clase "tb." lo que sea
tb = telebot.TeleBot(TOKEN,threaded=False,skip_pending=False)

# Los argumentos o parametros de ejecución haran que el bot nos avise de cada
# evento. Usamos el argumento 1 por que el argumento 0 es el nombre del archivo
# 1 -> para reinicios
# 2 -> para actualizaciones
# 3 -> para firmware


if sys.argv[1] == '1':
	# Requiere de otro argumento adicional. Pasa el nombre del archivo donde está
	# el registro de la actualización para poder enviarlo como adjunto.
	NombreArchivo =  sys.argv[2]
	tb.send_message(chatid,"Se ha reiniciado el equipo...")

	# Lee el archivo en modo binario (rb)
	archivo = open(NombreArchivo, 'rb')
	tb.send_document(chatid, archivo)

if sys.argv[1] == '2':
	# Requiere de otro argumento adicional. Pasa el nombre del archivo donde está
	# el registro de la actualización para poder enviarlo como adjunto.
	NombreArchivo =  sys.argv[2]
	tb.send_message(chatid,"Archivo de actualización de sistema...")

	# Lee el archivo en modo binario (rb)
	archivo = open(NombreArchivo, 'rb')
	tb.send_document(chatid, archivo)

	# Comprueba si se tiene que reiniciar algun servicio, requiere
	# la instalacion del paquete needrestart
	#reinicioServicios = os.popen("needrestart -r a").read()
	#tb.send_message(chatid,"Salida de needrestart -r a:\n\n"+reinicioServicios)

if sys.argv[1] == '3':
	# Requiere de otro argumento adicional. Pasa el nombre del archivo donde está
	# el registro de la actualización para poder enviarlo como adjunto.
	tb.send_message(chatid, "ATENCION\n\nEjecutar manualmente\n# rpi-update\n# sh /root/scripts/log_reinicio.sh")

if len(sys.argv) <= 1:
	# Envia un mensaje si se ha ejecutado el script sin parametros
	tb.send_message(chatid,"Se ha ejecutado el script del bot sin parámetros.")
